import React from 'react';
import {Controller, useForm} from 'react-hook-form';

export default function App() {

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    defaultValues: {
      firstName: '',
      lastName: '',
    }
  })

  const onSubmit = (data) => console.log(data)
}
