// AppNavigator.js

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet } from 'react-native';
import TabButton from './components/TabButton';
import LoginScreen from './screens/Login';
import ProfileScreen from './screens/Profile';
import MapScreen from './screens/Map';
import CargasCombustion from './screens/CargasCom';
import CarIfScreen from './screens/CarInf';
import ForgotScreen from './screens/Forgot';
import CambioPasswordScreen from './screens/Cambiopasswd';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const styles = StyleSheet.create({
  tabBar: {
    height: 70,
    position: 'absolute',
    bottom: 25,
    marginHorizontal: 16,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: '#dadada'
  }
});

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Main"
          component={MainNavigator}
          options={{ headerShown: false }} 
        />
        <Stack.Screen
          name="Forgot"
          component={ForgotScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Cambio"
          component={CambioPasswordScreen}
          options={{ headerShown: false }}
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
};

const MainNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: styles.tabBar,
        headerShown: false
      }}
    >
      <Tab.Screen 
        name="Profile" 
        component={ProfileScreen} 
        options={{
          tabBarButton: (props) => <TabButton {...props} item="Profile" />,
          tabBarIcon: ({ color, size, focused }) => (
            <Ionicons name={focused ? 'person-circle-outline' : 'person-circle'} size={size} color={color} />
          ),
          tabBarLabel: 'Profile'
        }}
      />
      <Tab.Screen 
        name="Map" 
        component={MapScreen} 
        options={{
          tabBarButton: (props) => <TabButton {...props} item="Map" />,
          tabBarIcon: ({ color, size, focused }) => (
            <Ionicons name={focused ? 'map-outline' : 'map'} size={size} color={color} />
          ),
          tabBarLabel: 'Map'
        }}
      />
      <Tab.Screen 
        name="Car" 
        component={CarIfScreen} 
        options={{
          tabBarButton: (props) => <TabButton {...props} item="Car" />,
          tabBarIcon: ({ color, size, focused }) => (
            <Ionicons name={focused ? 'car-outline' : 'car'} size={size} color={color} />
          ),
          tabBarLabel: 'Car'
        }}
      />
      <Tab.Screen
        name="Cargas"
        component={CargasCombustion}
        options={{
          tabBarButton: (props) => <TabButton {...props} item="Cargas" />,
          tabBarIcon: ({ color, size, focused }) => (
            <Ionicons name={focused ? 'local-gas-station' : 'cargas'} size={size} color={color} />
          ),
          tabBarLabel: 'Cargas'
        }}
      />
    </Tab.Navigator>
  );
};

export default AppNavigator;
