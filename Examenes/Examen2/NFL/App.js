import React, { useState, useEffect } from 'react';
import { View, Text, Button, FlatList } from 'react-native';

const App = () => {
  const [todos, setTodos] = useState([]);
  const [pendingTodos, setPendingTodos] = useState([]);
  const [completedTodos, setCompletedTodos] = useState([]);
  const [currentList, setCurrentList] = useState([]);
 
  useEffect(() => {
    fetchTodos();
  }, []);

  const fetchTodos = async () => {
    try {
      const response = await fetch('http://jsonplaceholder.typicode.com/todos');
      const data = await response.json();
      setTodos(data);
      filterTodos(data);
    } catch (error) {
      console.error('Error fetching todos:', error);
    }
  };

  const filterTodos = (todos) => {
    const pending = todos.filter(todo => !todo.completed);
    const completed = todos.filter(todo => todo.completed);
    setPendingTodos(pending);
    setCompletedTodos(completed);
  };

  const handleListSelection = (list) => {
    setCurrentList(list);
  };

  const renderTodoItem = ({ item }) => (
    <Text>{`${item.id} - ${item.title}`}</Text>
  );
  

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Pendientes de la NFL</Text>
      <Button title="Actualizar Lista" onPress={fetchTodos} color="pink" />

      <View style={{ marginTop: 10 }}>
        <Button title="Todos los Pendientes (solo IDs)" onPress={() => handleListSelection(todos.map(todo => ({ id: todo.id })))} color="pink"/>
      </View>

      <View style={{ marginTop: 10 }}>
        <Button title="Todos los Pendientes (IDs y Titles)" onPress={() => handleListSelection(todos)} color="pink"/>
      </View>

      <View style={{ marginTop: 10 }}>
        <Button title="Pendientes sin Resolver (ID y Title)" onPress={() => handleListSelection(pendingTodos)} color="pink"/>
      </View>

      <View style={{ marginTop: 10 }}>
        <Button title="Pendientes Resueltos (ID y Title)" onPress={() => handleListSelection(completedTodos)} color="pink"/>
      </View>

      <View style={{ marginTop: 10 }}>
        <Button title="Pendientes (IDs y userID)" onPress={() => handleListSelection(todos.map(todo => ({ id: todo.id, userId: todo.userId })))} color="pink"/>
      </View>

      <View style={{ marginTop: 10 }}>
        <Button title="Pendientes Resueltos (ID y userID)" onPress={() => handleListSelection(completedTodos.map(todo => ({ id: todo.id, userId: todo.userId })))} color="pink"/>
      </View>

      <View style={{ marginTop: 10 }}>
        <Button title="Pendientes sin Resolver (ID y userID)" onPress={() => handleListSelection(pendingTodos.map(todo => ({ id: todo.id, userId: todo.userId })))} color="pink"/>
      </View>

      <Text>Lista Seleccionada:</Text>
      <FlatList
        data={currentList}
        renderItem={renderTodoItem}
        keyExtractor={item => item.id.toString()}
      />
    </View>
  );
};

export default App;
