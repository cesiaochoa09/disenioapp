cesia ochoa <cesiaochoa09@gmail.com>
	
11:29 p.m. (hace 0 minutos)
	
para 0322104047
APP CODIGO 
import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View } from 'react-native';
import Post from './components/Post';
import Products from './components/Products';


export default function App() {
  return (
    <View style={styles.container}>
      {<Products/>}
     
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});